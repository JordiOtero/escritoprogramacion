<?php
$productos = $parametros['productos'];
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Comprar</title>
</head>

<body>
    <form action="/compras/procesarComprar" method="POST">
        <label for="producto">Productos:</label>
        <select name="producto">
            <?php foreach ($productos as $producto) {
                if ($producto->getStock() > 0) { ?>
                    <option value="<?php echo $producto->getId(); ?>"><?php echo $producto->getNombre(); ?></option>
            <?php }
            } ?>
        </select>
        <label for="cantidad">Cantidad:</label>
        <input name="cantidad" placeholder='cantidad' type="number" />
        <input type="submit" value="Guardar">
    </form>
</body>

</html>