<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Crear Producto</title>
</head>
<body>
    <form action="/productos/procesarCrear" method="POST">
        <label for="nombre">Nombre:</label>
        <input type="text" name="nombre" placeholder="Nombre" />
        <label for="descripcion">Descripcion:</label>
        <input type="text" name="descripcion" placeholder="Descripcion" />
        <label for="stock">Stock:</label>
        <input type="number" name="stock" placeholder="Stock" />
        <label for="precio">Precio:</label>
        <input type="number" name="precio" placeholder="Precio" />
        <input type="submit" value="Guardar">
    </form>
</body>
</html>