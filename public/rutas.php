<?php
require $_SERVER["DOCUMENT_ROOT"] . "/../routes/routes.class.php";
Routes::Add("/", "get", "HomeControlador::home");
Routes::Add("/login", "post", "HomeControlador::login");
Routes::Add("/productos", "get", "ProductoControlador::principal");
Routes::Add("/productos/editar", "get", "ProductoControlador::editar");
Routes::Add("/productos/procesarEditar", "post", "ProductoControlador::procesarEditar");
Routes::Add("/productos/eliminar", "get", "ProductoControlador::eliminar");
Routes::Add("/productos/crear", "get", "ProductoControlador::crear");
Routes::Add("/productos/procesarCrear", "post", "ProductoControlador::procesarCrear");
Routes::Add("/compras/comprar", "get", "CompraControlador::comprar");
Routes::Add("/compras/procesarComprar", "post", "CompraControlador::procesarComprar");
Routes::Add("/compras/listar", "get", "CompraControlador::listar");

Routes::Run();
