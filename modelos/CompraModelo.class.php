<?php
include_once('Modelo.class.php');


class CompraModelo extends Modelo
{
    private $id;
    private $producto;
    private $cantidad;
    private $fechaDeCompra;

    public function setProducto(int $producto)
    {
        $this->producto = $producto;
    }

    public function setId(int $id)
    {
        $this->id = $id;
    }

    public function setCantidad(int $cantidad)
    {
        $this->cantidad = $cantidad;
    }

    public function setFechaDeCompra(DateTime $fechaDeCompra)
    {
        $this->fechaDeCompra = $fechaDeCompra;
    }

    public function getProducto(): string
    {
        return $this->producto;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCantidad(): int
    {
        return $this->cantidad;
    }

    public function getFechaDeCompra(): DateTime
    {
        return $this->fechaDeCompra;
    }

    public function guardar()
    {

        $sql = "INSERT INTO compra(producto,cantidad,fechaDeCompra) VALUES(
                '{$this->getProducto()}',
                '{$this->getCantidad()}',
                '{$this->getFechaDeCompra()->format('Y-m-d H:i:s')}');";
        $this->conexion->query($sql);
        $this->setId($this->conexion->insert_id);
        return true;
    }

    public function listar()
    {
        $sql = "SELECT * FROM compra";
        $resultado = $this->conexion->query($sql);
        return $this->getArrayCompras($resultado);
    }

    private function getArrayCompras($compras)
    {
        $coleccionCompras = [];
        while ($row = $compras->fetch_assoc()) {
            $p = new self();
            $p->setId($row['id']);
            $p->setProducto($row['producto']);
            $p->setCantidad($row['cantidad']);
            $p->setFechaDeCompra(new Datetime($row['fechaDeCompra']));
            $coleccionCompras[] = $p;
        }
        return $coleccionCompras;
    }
}
