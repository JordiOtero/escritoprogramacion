<?php

use PHPUnit\Framework\TestCase;

class ProductoTest extends TestCase
{
    public function listado()
    {
        $productos = new ProductoModelo();
        $this->assertIsBool($productos instanceof ProductoModelo);
    }

    public function alta()
    {
        $producto = new ProductoModelo();
        $producto->setStock(20);
        $producto->setFechaAlta(new DateTime());
        $producto->setNombre('test');
        $producto->setDescripcion('test');
        $producto->setPrecio(200);
        $this->assertIsBool($producto->guardar());
    }
}
